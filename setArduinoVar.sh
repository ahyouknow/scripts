#!/bin/sh

info=$(arduino-cli board list | awk 'NR==2')
echo $info | grep -q Unknown && echo "board not plugged in" && return 1
port=$(echo $info | awk '{print $1}')
FQBN=$(echo $info | tr " " "\n" | awk '/[a-z]+:[a-z]+:[a-z]+$/')
export port
export FQBN
